if [ -z "$SESSION" ]; then
	SESSION=$(mktemp --directory);
	trap "rm -rf $SESSION" EXIT;
else
	CHILD=1;
fi

session_create() {
	cp -r "$SESSION" "$SESSION!";
	mv "$SESSION!" "$SESSION/!";
	SESSION="$SESSION/!";

	# Create an inline ws for the scripts' convenience.
	echo "$(realpath $DIR/ws) --session $SESSION \"\$@\"" > "$SESSION/ws";
	echo "$(realpath $DIR/ws) --session $SESSION --log \"\$@\"" > "$SESSION/log";

	chmod +x "$SESSION/log" "$SESSION/ws";
}

session_destroy() {
	SESSION="$(dirname $SESSION)";
	rm -rf "$SESSION/!";
}
