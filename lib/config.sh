# Some special strings such as $HOME or $WS will get converted to their respective
# real paths as follows:
# $HOME -> $HOME
# $WS -> $DIR
convert_special() {
	awk '{ gsub(/\$HOME/, "'"$HOME"'", $0); gsub(/\$WS/, "'"$(realpath "$DIR")"'", $0); print $0 }';
}

config() {
	type="--default";
	while [ $# -gt 0 ]; do
		case $1 in
			--default|--fallback|--optional)
				type="$1";
				;;
			--persistent)
				type="$1";
				;;
			--)
				shift;
				break;
				;;
			--*)
				fail "Unknown command: $1";
				;;
			*)
				break;
				;;
		esac
		shift;
	done

	name=$1;
	[ -n "$name" ] || fail "Config needs an argument.";
	if [ "$type" != "--persistent" ]; then
		config="$SESSION/$name";
	else
		config="$DIR/config/$name";
	fi

	shift;

	while [ $# -gt 0 ]; do
		case $1 in
			=)
				shift;
				dir=$(dirname "$config");
				[ -d "$dir" ] || mkdir -p "$dir";
				echo "$@" > "$config";
				return;
				;;
			*)
				fail "Unknown argument: $1";
				;;
		esac
		shift;
	done

	# Try global config (read-only)
	if [ ! -f "$config" ]; then
		config="$DIR/config/$name";
	fi

	if [ ! -f "$config" ]; then
		case $type in # HELP Config commands
			--default) # Default --config behaviour.
				# Will print an error message if ws/config/<variable> does not exist.
				error "Config file $config does not exist, nor was '$name' defined!";
				fail "Argument required: [1m$name[m";
				;;
			--fallback) # Use another config if this fails.
				# Will not print an error message if ws/config/<variable> does not
				# exist, given there are no subsequent errors.
				# This is useful when there are multiple variables a package can get
				# configuration from, and it does not matter if one of them is
				# missing. Should still print an error message if ALL configuration
				# files are missing.
				warn "Falling back from $name";
				# TODO: Make sure this creates a record for --default later on.
				# In case --default fails, we want a "trace" of previous configs we
				# fell back on, as any of these could have been defined.
				;;
			--optional) # Do not care if ws/config/<variable> does not exist.
				# This will still fail the script, but does not print any error.
				# It is up to the package to decide what to do with this variable.
				;;
		esac # END HELP
		exit 1;

	fi

	cat "$config" | convert_special;
}
