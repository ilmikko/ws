[ -n "$VERBOSITY" ] || VERBOSITY=0;

error() {
	pad "[31;1me:[m" $@;
}

fail() {
	error $@;
	exit 1;
}

log() {
	[ "$VERBOSITY" -ge "3" ] && pad $@;
}

pad() {
	awk 'BEGIN { for (i=0;i<'$DEPTH';i++) { printf("  ") } }' 1>&4;
	echo "$@" 1>&4;
}

success() {
	[ "$VERBOSITY" -ge "1" ] && pad "[32;1mOK:[m" $@;
}

user() {
	pad "[1m>" $@ "[m";
}

warn() {
	[ "$VERBOSITY" -ge "1" ] && pad "[33;1mw:[m" $@;
}
