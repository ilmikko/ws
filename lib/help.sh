usage() {
	echo "Usage: $(basename $0) [<action>] <package> [<argument> [<argument> [...]]]" 1>&2;
}

help_parse() {
	awk '/^\s*[^()]*)\s*#\s*\w*/ {
	cmd=$0;
	desc=$0;
	extraline=NR+1;

	sub(/^\s*/,"",cmd);
	sub(/)\s*#.*$/,"",cmd);
	sub(/^[^#]*#\s*/,"",desc);

	printf("%-11s%-20s\n", cmd, desc);
}
/^\s*#.*$/ {
if (extraline==NR) {
	desc=$0;

	sub(/^\s*#\s*/,"",desc);

	printf("%-11s%-20s\n", "", desc);
	extraline=NR+1;
}
}
/# H\ELP/ {
sub(/^.*#\s*HELP\s*/,"",$0);
title=$0;
printf("\n[1m%s[m\n", title);
help=1;
}
/# END H\ELP/ {
help=0;
}';
}

help() {
	usage;
	echo;
	echo "$(basename $0) is a tool to help build and maintain a workspace.";
	echo "Basic usage is always to perform operations on executable packages.";
	echo "These can be found from $DIR/packages.";
	cat $DIR/ws | help_parse;
	cat $DIR/lib.sh | help_parse;
	cat $DIR/lib/*.sh | help_parse;
}
