useradd() {
	echo "useradd $@" 1>&2;
}

id() {
	case $1 in
		test_user|1000)
			return 0;
			;;
		*)
			return 1;
			;;
	esac
	echo "id $@" 1>&2;
}

ws() {
	case $1 in
		--config)
			case $2 in
				--optional)
					case $3 in
						uid)
							echo "$USERID";
							return;
							;;
						username)
							echo "$USER";
							return;
							;;
					esac
					;;
			esac
			;;
	esac
	echo "ws $@" 1>&2;
}

USER=test_user;
USERID="";

X=$(. "packages/user/CHECK");
EC=$?;
if [ $EC != 0 ]; then
	echo "$X";
	exit 1;
fi

USER=test_user;
USERID="1000";

X=$(. "packages/user/CHECK");
EC=$?;
if [ $EC != 0 ]; then
	echo "$X";
	exit 1;
fi

USER=nonexistent_user;
USERID="1001";

X=$(. "packages/user/CHECK");
EC=$?;
if [ $EC != 1 ]; then
	echo "$X";
	exit 1;
fi

USER="";
USERID="1001";

X=$(. "packages/user/CHECK");
EC=$?;
if [ $EC != 1 ]; then
	echo "$X";
	exit 1;
fi
