touch FILE;
trap "rm FILE" EXIT;

ws() {
	case $1 in
		--config)
			case $2 in
				file)
					echo "FILE";
					return;
					;;
				match)
					echo "$MATCH";
					return;
					;;
				replacement)
					echo "$REPLACEMENT";
					return;
					;;
			esac
			;;
	esac
	echo $@ 1>&2;
}

test_line_exists_check() {
	cat >FILE <<-HERE
	Hello world
	This is a file with lines
	Please take care with these lines
	Goodbye
	HERE

	MATCH="is a file with";
	REPLACEMENT="This is a file with lines";

	X=$(. "packages/file_replace_line/CHECK");
	EC=$?;
	if [ $EC != 0 ]; then
		echo "$X";
		exit 1;
	fi
}

test_line_missing_check() {
	cat >FILE <<-HERE
	Hello world
	This is a file with lines
	Please take care with these lines
	Goodbye
	HERE

	MATCH="is a file with";
	REPLACEMENT="This is a present with lines";

	X=$(. "packages/file_replace_line/CHECK");
	EC=$?;
	if [ $EC != 1 ]; then
		echo "$X";
		exit 1;
	fi
}

test_line_exists_check_multiline() {
	cat >FILE <<-HERE
	Hello world
	This is a file with lines
	Please take care with these lines
	Goodbye
	HERE

	MATCH="is a file with";
	REPLACEMENT="$(cat <<-HERE
	This is a file with lines
	Please take care with these lines
	HERE
	)";

	X=$(. "packages/file_replace_line/CHECK");
	EC=$?;
	if [ $EC != 0 ]; then
		echo "$X";
		exit 1;
	fi
}


test_line_missing_check_multiline() {
	cat >FILE <<-HERE
	Hello world
	This is a file with lines
	Please take care with these lines
	Goodbye
	HERE

	MATCH="is a file with";
	REPLACEMENT="$(cat <<-HERE
	This is a file with lines
	Please take care with these asd
	HERE
	)";

	X=$(. "packages/file_replace_line/CHECK");
	EC=$?;
	if [ $EC != 1 ]; then
		echo "$X";
		exit 1;
	fi
}

test_line_replace() {
	cat >FILE <<-HERE
	Hello world
	This is a file with lines
	Please take care with these lines
	Goodbye
	HERE

	MATCH="This is a file";
	REPLACEMENT="This is a present";

	. "packages/file_replace_line/RUN";

	! ! assert_equals "$(cat FILE)" "$(cat <<-HERE
	Hello world
	This is a present
	Please take care with these lines
	Goodbye
	HERE
	)" || exit 1;
}

test_line_replace_single() {
	cat >FILE <<-HERE
	Hello world
	This is a file with these lines
	Please take care with these lines
	Goodbye
	HERE

	MATCH="with these lines";
	REPLACEMENT="XXX YYY ZZZ";

	. "packages/file_replace_line/RUN";

	! ! assert_equals "$(cat FILE)" "$(cat <<-HERE
	Hello world
	XXX YYY ZZZ
	Please take care with these lines
	Goodbye
	HERE
	)" || exit 1;
}

test_line_replace_whole() {
	cat >FILE <<-HERE
	Hello world
	This is a file with lines
	Please take care with these lines
	Goodbye
	HERE

	MATCH="is a file with";
	REPLACEMENT="This is a present with lines";

	. "packages/file_replace_line/RUN";

	! ! assert_equals "$(cat FILE)" "$(cat <<-HERE
	Hello world
	This is a present with lines
	Please take care with these lines
	Goodbye
	HERE
	)" || exit 1;
}

test_line_replace_multiline() {
	cat >FILE <<-HERE
	# Some comments
	#[multilib]
	#/etc/pacman.conf
	#command command
	#command2 command2
	# Some other comments
	HERE

	MATCH="$(cat <<-HERE
	#[multilib]
	#/etc/pacman.conf
	#command command
	#command2 command2
	HERE
	)";
	REPLACEMENT="$(cat <<-HERE
	[multilib]
	/etc/pacman.conf
	command command
	HERE
	)";

	. "packages/file_replace_line/RUN";

	! ! assert_equals "$(cat FILE)" "$(cat <<-HERE
	# Some comments
	[multilib]
	/etc/pacman.conf
	command command
	# Some other comments
	HERE
	)" || exit 1;
}

test_line_replace_multiline_no_match() {
	cat >FILE <<-HERE
	# Some comments
	#[multilib]
	#/etc/pacman.conf
	#command command
	#command2 command2
	# Some other comments
	# Comments
	#[multilib]
	#/etc/pacman.conf
	#command command
	#command3 command3
	HERE

	MATCH="$(cat <<-HERE
	#[multilib]
	#/etc/pacman.conf
	#command command
	#command4 command4
	HERE
	)";
	REPLACEMENT="$(cat <<-HERE
	[multilib]
	/etc/pacman.conf
	command command
	command4 command4
	HERE
	)";

	. "packages/file_replace_line/RUN";

	! ! assert_equals "$(cat FILE)" "$(cat <<-HERE
	# Some comments
	#[multilib]
	#/etc/pacman.conf
	#command command
	#command2 command2
	# Some other comments
	# Comments
	#[multilib]
	#/etc/pacman.conf
	#command command
	#command3 command3
	[multilib]
	/etc/pacman.conf
	command command
	command4 command4
	HERE
	)" || exit 1;
}

test_line_replace_no_match() {
	cat >FILE <<-HERE
	Hello world
	This is a file with lines
	Please take care with these lines
	Goodbye
	HERE

	MATCH="A B C D E F G";
	REPLACEMENT="A new line to the file!";

	X=$(. "packages/file_replace_line/RUN") || exit 1;

	! ! assert_equals "$(cat FILE)" "$(cat <<-HERE
	Hello world
	This is a file with lines
	Please take care with these lines
	Goodbye
	A new line to the file!
	HERE
	)" || exit 1;
}

test_line_replace_block() {
	cat >FILE <<-HERE
	Hello world
	This is a file with lines
	Please take care with these lines
	Goodbye
	HERE

	MATCH="This is a file";
	REPLACEMENT="$(cat <<-HERE
	!! This is a public service announcement!
	!! I have been recently informed that
	!! blocks of code have been declared illegal.
	!! That is all, good day.
	HERE
	)";

	. "packages/file_replace_line/RUN";

	! ! assert_equals "$(cat FILE)" "$(cat <<-HERE
	Hello world
	!! This is a public service announcement!
	!! I have been recently informed that
	!! blocks of code have been declared illegal.
	!! That is all, good day.
	Please take care with these lines
	Goodbye
	HERE
	)" || exit 1;
}
