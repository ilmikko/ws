#!/usr/bin/env bash
assert_equals "$(./ws device_name /dev/sda 1)"		"/dev/sda1";
assert_equals "$(./ws device_name /dev/sda 3)"		"/dev/sda3";
assert_equals "$(./ws device_name /dev/sdc 5)"		"/dev/sdc5";
assert_equals "$(./ws device_name /dev/mmcblk0 3)"	"/dev/mmcblk0p3";
assert_equals "$(./ws device_name /dev/mmcblk1 11)"	"/dev/mmcblk1p11";
assert_equals "$(./ws device_name /dev/mmcblk11 1)"	"/dev/mmcblk11p1";
