. "packages/install_git/LIB" || exit 1;

test_get_repository() {
	assert_equals "$(echo "gitlab.com/ilmikko/test" | get_repository)" "https://gitlab.com/ilmikko/test";
	assert_equals "$(echo "http://gitlab.com/ilmikko/test" | get_repository)" "https://gitlab.com/ilmikko/test";
	assert_equals "$(echo "https://gitlab.com/ilmikko/test" | get_repository)" "https://gitlab.com/ilmikko/test";
	assert_equals "$(echo "https://gitlab.com/ilmikko/test/other/dir" | get_repository)" "https://gitlab.com/ilmikko/test";
	assert_equals "$(echo "gitlab.com/ilmikko/test/other/dir" | get_repository)" "https://gitlab.com/ilmikko/test";
}

test_get_subdir() {
	assert_equals "$(echo "gitlab.com/ilmikko/test/subdir" | get_subdir)" "/subdir";
	assert_equals "$(echo "gitlab.com/ilmikko/test/subdir/asd" | get_subdir)" "/subdir/asd";
	assert_equals "$(echo "https://gitlab.com/ilmikko/test/subdir/asd" | get_subdir)" "/subdir/asd";
	assert_equals "$(echo "gitlab.com/ilmikko/test" | get_subdir)" "";
	assert_equals "$(echo "http://gitlab.com/ilmikko/test/" | get_subdir)" "/";
}
