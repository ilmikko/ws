#!/usr/bin/env bash

summary() {
	awk '/^pkgdesc/ { gsub(/^[^'"'"'"]*['"'"'"]|['"'"'"])?$/,"",$0); desc=$0 } /^license/ { gsub(/^[^'"'"'"]*['"'"'"]|['"'"'"])?$/,"",$0); license=$0 } /^source/ { gsub(/^[^'"'"'"]*['"'"'"]|['"'"'"])?$/,"",$0); source=$0 } END { print desc, "("license")"; print "[1mSource:",source,"[m" }';
}

pkgver() {
	awk '/^pkgver/ { sub(/^[^=]*=/,"",$1); gsub(/^['"'"'"]|['"'"'"]$/,"",$1); print $1 }';
}

pkgrel() {
	awk '/^pkgrel/ { sub(/^[^=]*=/,"",$1); gsub(/^['"'"'"]|['"'"'"]$/,"",$1); print $1 }';
}

pkgname() {
	awk '/^pkgname/ { sub(/^[^=]*=/,"",$1); gsub(/^['"'"'"]|['"'"'"]$/,"",$1); print $1 }';
}

dependencies() {
	awk '/^depends/ { sub(/^[^=]*=/,"",$1); gsub(/^\(|\)$/,"",$0); print $0 }' | awk '{ for (i=1;i<=NF;i++) { gsub(/^['"'"'"]|['"'"'"]$/,"",$i); printf("%s ",$i); } }'
}

package=$(ws --config package_name);
[ -n "$package" ] || exit 1;

lib=$(ws --config --fallback dir/lib);
[ -n "$lib" ] || lib="$(mktemp --directory)";

# Trust can currently be either "full" or "none".
# If the user trusts the package, it will get installed.
# If they do not trust the package (default), the PKGBUILD is simply inspected.
# We could also inspect the website for comments and such.
trust=$(ws --config --fallback trust);
[ -n "$trust" ] || trust="none";

mkdir -p "$lib";
cd "$lib";
ws -do fetch_git "remote=https://aur.archlinux.org/$package.git" "local=$lib/$package" || exit 1;

cd "$lib/$package";
if [ ! -f "$lib/$package/PKGBUILD" ]; then
	log --error "$lib/$package does not appear to be a package.";
	log --error "Cannot find PKGBUILD!";
	exit 1;
fi

case $trust in
	none)
		cat "PKGBUILD";
		cat "PKGBUILD" | summary;
		# Handy AWK to parse comments from the AUR.
		curl --silent "https://aur.archlinux.org/packages/$package/" | awk '/div class.*comments-header/ { comments=1 } /<\/h4>/ { header=0; } { $1=$1; if (header && $0 && comments) { printf "\n[34;1m"$1"[m"; $1=""; print "[1m"$0"[m" } } /comment-header/ { header=1; content=0; } { gsub(/<\/?[a-z ]*\/?>/,"",$0); if (content && comments && $0) print $0 } /comment-.*-content/ { content=1; } /script/ { comments=0 }';
		exit 1;
		;;
	full)
		cat "PKGBUILD" | summary;
		sleep 1;

		for file in *".pkg.tar.xz"; do
			[ -f "$file" ] || continue;
			pkgname=$file;
		done

		if [ -z "$pkgname" ]; then
			# We need to be not root in order to make.
			if [ "$UID" = 0 ]; then
				# Install dependencies first.
				pacman --sync --noconfirm $(cat "PKGBUILD" | dependencies) || exit 1;

				# We cannot use $HOME for root, because we will need to not be a root user to
				# make the package, and that user will not have access to the root dir.
				ws -do user "username=notroot" || exit 1;

				tempdir=$(mktemp --directory);
				cp -r "$lib/$package/." "$tempdir";
				chown "notroot:notroot" "$tempdir";
				(
					cd "$tempdir";
					su --command "makepkg -c" notroot || exit 1;
				)
				cp -r "$tempdir/." "$lib/$package";
			else
				makepkg -sc || exit 1;
			fi
		else
			echo "Package file already found: $pkgname";
		fi

		for file in *".pkg.tar.xz"; do
			[ -f "$file" ] || continue;
			pkgname=$file;
		done

		if [ "$UID" = 0 ]; then
			pacman --upgrade --noconfirm "$pkgname";
		else
			ws -do package sudo || exit 1;
			sudo pacman --upgrade --noconfirm "$pkgname";
		fi
		;;
	*)
		log --error "Trust can either be 'none' or 'full'.";
		log --error "Unknown trust: $trust";
		exit 1;
		;;
esac
