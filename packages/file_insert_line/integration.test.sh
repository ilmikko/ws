touch FILE;
trap "rm FILE" EXIT;

log() {
	echo $@ 1>&2;
}

ws() {
	case $1 in
		--config)
			case $2 in
				--fallback)
					case $3 in
						after)
							echo "$AFTER";
							return;
							;;
						before)
							echo "$BEFORE";
							return;
							;;
					esac
					;;
				file)
					echo "FILE";
					return;
					;;
				line)
					echo "$LINE";
					return;
					;;
			esac
			;;
	esac
	echo $@ 1>&2;
}

test_line_exists_check() {
	cat >FILE <<-HERE
	Hello world
	This is a file with lines
	Please take care with these lines
	Goodbye
	HERE

	LINE="This is a file with lines";

	X=$(. "packages/file_insert_line/CHECK");
	EC=$?;
	if [ $EC != 0 ]; then
		echo "$X";
		exit 1;
	fi
}

test_line_missing_check() {
	cat >FILE <<-HERE
	Hello world
	This is a file with lines
	Please take care with these lines
	Goodbye
	HERE

	LINE="Oh no this line does not exist!";

	X=$(. "packages/file_insert_line/CHECK");
	EC=$?;
	if [ $EC != 1 ]; then
		echo "$X";
		exit 1;
	fi

	LINE="This is a file with";

	X=$(. "packages/file_insert_line/CHECK");
	EC=$?;
	if [ $EC != 1 ]; then
		echo "$X";
		exit 1;
	fi
}

test_line_insert() {
	cat >FILE <<-HERE
	Hello world
	This is a file with lines
	Please take care with these lines
	Goodbye
	HERE

	LINE="The new file to insert";

	. "packages/file_insert_line/RUN";

	! ! assert_equals "$(cat FILE)" "$(cat <<-HERE
	Hello world
	This is a file with lines
	Please take care with these lines
	Goodbye
	The new file to insert
	HERE
	)" || exit 1;
}

test_line_insert_after() {
	cat >FILE <<-HERE
	Hello world
	This is a file with lines
	Please take care with these lines
	Goodbye
	HERE

	LINE="The new file to insert";
	AFTER="Hello world";
	BEFORE="";

	. "packages/file_insert_line/RUN";

	! ! assert_equals "$(cat FILE)" "$(cat <<-HERE
	Hello world
	The new file to insert
	This is a file with lines
	Please take care with these lines
	Goodbye
	HERE
	)" || exit 1;
}

test_line_insert_before() {
	cat >FILE <<-HERE
	Hello world
	This is a file with lines
	Please take care with these lines
	Goodbye
	HERE

	LINE="The new file to insert";
	AFTER="";
	BEFORE="Hello world";

	. "packages/file_insert_line/RUN";

	! ! assert_equals "$(cat FILE)" "$(cat <<-HERE
	The new file to insert
	Hello world
	This is a file with lines
	Please take care with these lines
	Goodbye
	HERE
	)" || exit 1;
}
