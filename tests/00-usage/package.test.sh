# --package should show whether a package is installed or not.
assert_success "./ws --package mount";
assert_success "./ws --package unmount";
assert_fail "./ws --package nonexistent";
