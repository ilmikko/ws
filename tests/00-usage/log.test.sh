# ws --log should log into the stderr.
assert_success "./ws --log hello world";
assert_equals "$(./ws -vvvv --log hello world)" ""
assert_equals "$(./ws -vvvv --log hello world 2>&1)" "hello world";
