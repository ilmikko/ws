# Test that the help page contains the necessary information.
assert_fail "./ws";
assert_exec_find "./ws --help" "Usage: ws";

assert_success "./ws --help";
assert_exec_find "./ws --help" "Usage: ws" "a tool to help build and maintain a workspace" "perform operations on executable packages";
