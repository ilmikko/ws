# --list should succeed and return a list of packages.

assert_success "./ws --list";
assert_find "$(./ws --list)" "mount" "unmount" "lock" "unlock";
