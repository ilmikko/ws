# Test that whitespace is dealt properly in ws configs.
test_regular_config() {
	./ws --config --persistent spaces = This has some spaces;
	assert_equals "$(./ws --config --persistent spaces)" "This has some spaces";
	./ws --config --persistent spaces = "This also has some spaces";
	assert_equals "$(./ws --config --persistent spaces)" "This also has some spaces";

	./ws --config --persistent tabs = This	has		some			tabs;
	assert_equals "$(./ws --config --persistent tabs)" "This has some tabs";
	./ws --config --persistent tabs = "This	also		has			some				tabs";
	assert_equals "$(./ws --config --persistent tabs)" "This	also		has			some				tabs";

	./ws --config --persistent linebreaks = "$(printf "This\nhas\nsome\nline\nbreaks")";
	! ! assert_equals "$(./ws --config --persistent linebreaks)" "$(cat <<-HERE
	This
	has
	some
	line
	breaks
	HERE
	)" || exit 1;
	./ws --config --persistent linebreaks = "$(cat <<-HERE
	This
	also
	has
	some
	line
	breaks
	HERE
	)";
	! ! assert_equals "$(./ws --config --persistent linebreaks)" "$(cat <<-HERE
	This
	also
	has
	some
	line
	breaks
	HERE
	)" || exit 1;
}

test_inline_package_config() {
	assert_equals "$(./ws echo_package "value=This has some spaces")" "This has some spaces";
	# assert_equals "$(./ws echo_package "This has some spaces")" "This has some spaces";

	assert_equals "$(./ws echo_package "value=This	has		some			tabs")" "This	has		some			tabs";

	assert_equals "$(./ws echo_package "value=$(printf "This\nhas\nsome\nline\nbreaks")")" "$(printf "This\nhas\nsome\nline\nbreaks")";

	! ! assert_equals "$(./ws echo_package "value=$(cat <<-HERE
	This
	has
	some
	line
	breaks
	HERE
	)")" "$(cat <<-HERE
	This
	has
	some
	line
	breaks
	HERE
	)" || exit 1;
}

test_inline_package_special() {
	! ! assert_equals "$(./ws echo_package "value=$(cat <<-HERE
	This = package
	has = some
	some = some
	line = line
	breaks = breaks
	HERE
	)")" "$(cat <<-HERE
	This = package
	has = some
	some = some
	line = line
	breaks = breaks
	HERE
	)" || exit 1;
}
