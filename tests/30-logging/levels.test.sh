# Test that logging levels work as expected.

# Log levels:
# 0. echo
# 1. log/stderr
# 2. warn/success
# 3. error/user

! ! assert_equals "$(./ws ./logger 2>&1)" "$(cat <<HERE
This is an echo line.
  [1m> This is a user log line. [m
  [31;1me:[m This is an error log line.
HERE
)" || exit 1;

! ! assert_equals "$(./ws ./logger 2>/dev/null)" "$(cat <<HERE
This is an echo line.
HERE
)" || exit 1;

! ! assert_equals "$(./ws -v ./logger 2>&1)" "$(cat <<HERE
This is an echo line.
  [1m> This is a user log line. [m
  [33;1mw:[m This is a warn log line.
  [31;1me:[m This is an error log line.
  [32;1mOK:[m This is a success log line.
HERE
)" || exit 1;

! ! assert_equals "$(./ws -vv ./logger 2>&1)" "$(cat <<HERE
This is an echo line.
  [1m> This is a user log line. [m
  [33;1mw:[m This is a warn log line.
  [31;1me:[m This is an error log line.
  [32;1mOK:[m This is a success log line.
This is echo to stderr.
HERE
)" || exit 1;

! ! assert_equals "$(./ws -vvv ./logger 2>&1)" "$(cat <<HERE
RUN logger
  This is a log line.
This is an echo line.
  [1m> This is a user log line. [m
  [33;1mw:[m This is a warn log line.
  [31;1me:[m This is an error log line.
  [32;1mOK:[m This is a success log line.
This is echo to stderr.
-> 0
HERE
)" || exit 1;

! ! assert_equals "$(./ws -vvvv ./logger 2>&1)" "$(cat <<HERE
RUN logger
  This is a log line.
This is an echo line.
  [1m> This is a user log line. [m
  [33;1mw:[m This is a warn log line.
  [31;1me:[m This is an error log line.
  [32;1mOK:[m This is a success log line.
This is standard echo.
This is echo to stderr.
-> 0
HERE
)" || exit 1;

! ! assert_equals "$(./ws -vvvv ./logger 2>/dev/null)" "$(cat <<HERE
This is an echo line.
HERE
)" || exit 1;
