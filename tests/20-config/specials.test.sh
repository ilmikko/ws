# Some paths are special.
# Make sure $WS is translated to wherever ws is installed,
# and that $HOME is translated to $HOME respectively.

test_home() {
	./ws --config --persistent myhome = \$HOME;
	assert_equals "$(./ws --config myhome)" "$HOME";

	./ws --config --persistent myvimwiki = \$HOME/vimwiki;
	assert_equals "$(./ws --config myvimwiki)" "$HOME/vimwiki";

	./ws --config --persistent double = \$HOME/\$HOME/HOME;
	assert_equals "$(./ws --config double)" "$HOME/$HOME/HOME";
}

test_ws() {
	DIR=$(dirname $0);
	./ws --config --persistent myws = \$WS;
	assert_equals "$(./ws --config myws)" "$DIR";

	./ws --config --persistent myconfig = \$WS/config;
	assert_equals "$(./ws --config myconfig)" "$DIR/config";

	./ws --config --persistent double = \$WS/\$WS/WS;
	assert_equals "$(./ws --config double)" "$DIR/$DIR/WS";
}
