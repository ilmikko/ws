# Test that comments are properly removed.

cat >./config/commented_file <<-HERE
# The below line says config = config
config = config
# The below line says config = config2
config = config2 # inline comment
HERE
# 
# ! ! assert_equals "$(./ws --config commented_file)" "$(cat <<-HERE
# config = config
# config = config2 # inline comment
# HERE
# )" || exit 1;

# However, if we set a config, comments should not be removed.
ws --config --persistent commented_file = "$(cat <<-HERE
# The below line says config = config
config = config
# The below line says config = config2
config = config2
HERE
)" || exit 1;

! ! assert_equals "$(./ws --config commented_file)" "$(cat <<-HERE
# The below line says config = config
config = config
# The below line says config = config2
config = config2 # inline comment
HERE
)" || exit 1;
