#!/usr/bin/env bash
# KISS

DIR=$(dirname $0);

fail() {
	echo $@ 1>&2;
	exit 1;
}

. "$DIR/lib.sh" || fail "Failed to find $DIR/lib.sh!";
. "$DIR/lib/help.sh" || fail "Failed to find $DIR/help.sh!";
. "$DIR/lib/config.sh" || fail "Failed to find $DIR/lib/config.sh!";
. "$DIR/lib/logging.sh" || fail "Failed to find $DIR/lib/logging.sh!";
. "$DIR/lib/session.sh" || fail "Failed to find $DIR/lib/session.sh!";

if [ $# = 0 ]; then
	usage;
	exit 1;
fi

while [ $# -gt 0 ]; do
	case "$1" in
		-vvvv)
			VERBOSITY=4;
			;;
		-vvv)
			VERBOSITY=3;
			;;
		-vv)
			VERBOSITY=2;
			;;
		-v)
			VERBOSITY=1;
			;;
		*)
			break;
			;;
	esac
	shift;
done

if [ -z "$CHILD" ]; then
	commands "$@" 3>&1 4>&2;
else
	commands "$@";
fi
