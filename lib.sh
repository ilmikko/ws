DEPTH=0;

assert_valid_file() {
	[ -f "$1" ] || fail "File $1 does not exist!";
	[ -x "$1" ] || fail "File $1 is not executable!";
}

exec_root() {
	assert_valid_file $file;

	# This only works if we are root already, or if we are in an interactive shell.
	if [ "$(id -u)" = "0" ]; then
		exec_file;
		return $?;
	fi

	if [ ! -t "1" ]; then
		log "Cannot run as root; not interactive!";
		exit 1;
	fi

	session_create;
	# Pass ws in PATH in case it wasn't passed already.
	if [ "$VERBOSITY" -ge "4" ]; then
		VERBOSITY="$VERBOSITY" PATH="$SESSION:$PATH" SESSION="$SESSION" DEPTH="$DEPTH" $file $@ 1>&4 2>&4;
	elif [ "$VERBOSITY" -ge "2" ]; then
		VERBOSITY="$VERBOSITY" PATH="$SESSION:$PATH" SESSION="$SESSION" DEPTH="$DEPTH" $file $@ 1>/dev/null 2>&4;
	else
		VERBOSITY="$VERBOSITY" PATH="$SESSION:$PATH" SESSION="$SESSION" DEPTH="$DEPTH" $file $@ 1>/dev/null 2>/dev/null;
	fi
	EC=$?;
	session_destroy;

	return $EC;
}

exec_file() {
	assert_valid_file $file;

	session_create;
	# We pass ws in PATH in case it wasn't passed already.
	if [ "$VERBOSITY" -ge "4" ]; then
		VERBOSITY="$VERBOSITY" PATH="$SESSION:$PATH" SESSION="$SESSION" DEPTH="$DEPTH" $file $@ 1>&4 2>&4;
	elif [ "$VERBOSITY" -ge "2" ]; then
		VERBOSITY="$VERBOSITY" PATH="$SESSION:$PATH" SESSION="$SESSION" DEPTH="$DEPTH" $file $@ 1>/dev/null 2>&4;
	else
		VERBOSITY="$VERBOSITY" PATH="$SESSION:$PATH" SESSION="$SESSION" DEPTH="$DEPTH" $file $@ 1>/dev/null 2>/dev/null;
	fi
	EC=$?;
	session_destroy;

	return $EC;
}

check() {
	file=$package/CHECK;
	name=$(basename $package);

	log "CHECK $name";

	exec_file;
	EC=$?;

	if [ $EC -gt 0 ]; then
		log "GO!";
		return 1;
	fi

	success "$name";
	return 0;
}

do_() {
	check || run;
}

run() {
	file=$package/RUN;

	log "RUN $(basename $package)";

	# Check if we need to be root.
	if [ -f "$package/ROOT" ]; then
		exec_root;
		EC=$?;
	else
		exec_file;
		EC=$?;
	fi

	if [ $EC -gt 0 ]; then
		error "-> $EC";
		fail "Installation of $file failed!";
	else
		log "-> $EC";
	fi
}

try() {
	# Like do, except doesn't complain about missing CHECK or RUN files.
	[ -f "$package/CHECK" ] && check || ([ -f "$package/RUN" ] && run);
}

action() {
	case "$1" in # HELP Actions
		-check) # CHECK the package.
			# A CHECK file is an executable which checks whether a RUN step is
			# necessary.
			# It will return an error code >0 if the CHECK fails, 0 if it passes.
			# You can use it in subsequent scripts.
			check;
			;;
		-do) # CHECK the package, RUN if the CHECK fails.
			# This is a composite command.
			# It is equivalent to:
			# ws -check <package> || ws -run <package>
			do_;
			;;
		-run) # Run the package RUN file.
			# You can pass -run <package> to ignore a CHECK and run it regardless of a
			# CHECK's outcome (the CHECK will not run).
			# A RUN file is an executable. It is used to perform a task.
			# If a CHECK file is present, a RUN file usually will make that CHECK
			# pass.
			# This is useful to not repeat execution in case it is not necessary.
			# For example, for decompressing a file, a CHECK file might check whether
			# a decompressed folder already exists.
			# If it does (the CHECK passes), a RUN file does not need to be executed.
			#
			run;
			;;
		-try) # Check and run if check exists, otherwise just run.
			# This is the same as -do, but a missing CHECK does not cause an error.
			# Instead, if a CHECK is missing, the package RUN file will only run.
			# This is the default option if action is not provided.
			try;
			;;
		*)
			fail "Unknown action: $1";
			;;
	esac # END HELP
}

commands() {
	while [ $# -gt 0 ]; do
		case "$1" in # HELP Commands
			# Basic command line commands.

			--config) # Get/set configuration.
			# You can use `--config id` to get a configuration file from ws/config/id.
			# In order to write an id, you can use `--config id = ...`.
			# This will write the value provided to ws/config/id.
			# In order to provide values to packages, you should use arguments (see below).
			shift;
			[ -n "$1" ] || fail "--config needs a second argument.";

			config "$@";
			exit;
			;;
		--help) # Display this help page.
			help;
			exit;
			;;
		--list) # Display installed package list.
			for package in "$DIR/packages/"*; do
				basename "$package";
			done
			exit;
			;;
		--log) # Log from a child script.
			# Usually there is no reason to call this yourself, unless you are
			# developing a module.
			shift;

			case $1 in
				--error)
					shift;
					error $@;
					;;
				--echo)
					shift;
					echo "$@" 1>&3;
					;;
				--success)
					shift;
					success $@;
					;;
				--user)
					shift;
					user $@;
					;;
				--warn)
					shift;
					warn $@;
					;;
				--)
					shift;
					log $@;
					;;
				--*)
					fail "Unknown command: $1";
					;;
				*)
					log $@;
					;;
			esac
			exit 0;
			;;
		--package) # Check if a package exists.
			# Returns the package location if it does.
			# Also returns an error code, 0 if a package exists, >0 if it does not.
			shift;
			[ -n "$1" ] || fail "--package needs a second argument.";

			[ -d "$DIR/packages/$1" ] && echo "$DIR/packages/$1" && exit 0;
			[ -x "$1/RUN" ] && echo "$1" && exit 0;

			exit 1;
			;;
		--session) # Set session ID.
			# Only to be used in scripts.
			shift;
			[ -n "$1" ] || fail "--session needs a second argument.";
			# A session ID is the path to the session's config folder.
			# Subprocesses called from a single ws call should usually have the same session ID.
			SESSION=$1;
			# We can also get the layer depth from our session ID.
			DEPTH=$(echo $1/ | awk -F! '{ for (i=0; i<=NF; i++) { if ($i=="/") depth+=1 } } END { print depth }');
			;;
		--*)
			fail "Unknown argument: $1";
			;;
		*|*=*)
			break;
			;;
	esac # END HELP
	shift;
done

main "$@";
}

main() {
	action="-try";
	while [ $# -gt 0 ]; do
		case "$1" in
			--*)
				;;
			-run|-check|-do|-try)
				action="$1";
				;;
			-*)
				fail "Unknown argument: $1";
				;;
			*)
				break;
				;;
		esac
		shift;
	done

	package="$1";
	[ -d "$package" ] && [ -x "$package/RUN" ] || package="$DIR/packages/$1";
	shift;

	[ -d "$package" ] || fail "Package $package does not exist!";
	[ -f "$package/META" ] && meta="$(cat "$package/META")";

	while [ $# -gt 0 ]; do
		case "$1" in # HELP Arguments
			*=*) # Basic named argument
				# You can define named arguments to pass to packages.
				# ws -run <package> argument=value
				# This is the equivalent to configuring ws/config/argument to
				# hold "value" beforehand.

				key=$(echo "$1" | awk -F= '{ print $1; exit }');
				val=$(echo "$1" | awk -F= '{ if (NR==1) { sub(/[^=]*=/, "", $0) } print $0 }');

				config "$key" = "$val";
				;;
			++*) # Silent input
				# In case you do not want to pass "password=..." in your command line
				# (hint: you shouldn't), you can use "++password" for password to be
				# requested on runtime.
				key=$(echo $1 | awk '{ print substr($0, 3) }');

				echo "Type $key and press enter:";

				stty -echo;
				read -s val;
				stty echo;

				config "$key" = "$val";
				;;
			+*) # User input
				# You can use +argument for an argument to be read during runtime.
				# If you don't want the characters to be echoed out back to the terminal
				# (for example in case you are typing passwords), you can use ++ instead
				# of +.
				key=$(echo $1 | awk '{ print substr($0, 2) }');

				printf "$key = ";

				read val;

				config "$key" = "$val";
				;;
			*) # Meta input
				# Any other input will be parsed according to the package's META file.
				# If the META file will not contain any additional values, an error will
				# be thrown.
				# For example,
				# ` -run unzip compressed.zip `
				# will translate into
				# ` -run unzip file=compressed.zip `
				# given the unzip package's META file contains 'file'.
				[ -n "$meta" ] || fail "Excess argument '$1'! Please check the META file or use named arguments.";

				next_key=$(echo $meta | awk '{ print $1 }');
				meta=$(echo $meta | awk '{ $1=""; print $0 }');

				config "$next_key" = "$1";
				;;
		esac # END HELP
		shift;
	done

	action "$action";
}
